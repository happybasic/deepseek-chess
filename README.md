# deepseek-chess

通过纯文本引导deepseek写的一个简单的中国象棋引擎。

**棋子含义**：

将/帅: K/k, 士: S/s, 象: X/x, 马: M/m, 车: C/c, 炮: P/p, 兵/卒: B/z
红方（大写）先走，黑方（小写）为 AI。

输入移动指令，例如：7 7 7 4 表示将红炮(P)从(7行,7列)移动到(7行,4列)。

![屏幕截图 2025-01-09 162150.jpg](https://raw.gitcode.com/happybasic/deepseek-chess/attachment/uploads/886412a4-672d-4906-9928-6745f8709675/屏幕截图_2025-01-09_162150.jpg '屏幕截图 2025-01-09 162150.jpg')